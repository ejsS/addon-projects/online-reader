/** 
 * Copyright 2015 - Félix J. García
 * 
 * @author Félix J. García (fgarcia@um.es)
 */

LibraryComPADRE = {
	OSP_INFO_URL: "http://www.compadre.org/OSP/online_help/EjsDL/OSPCollection.html",
	EJS_SERVER_TREE_LOCAL: "osp.php?basename=tree",
	EJS_SERVER_TREE: "http://www.compadre.org/services/rest/osp_ejss.cfm?verb=Identify&OSPType=EJSS+Model",
	EJS_SERVER_RECORDS_LOCAL: "osp.php?basename=records",
	EJS_SERVER_RECORDS: "http://www.compadre.org/osp/services/REST/osp_ejss.cfm?OSPType=EJSS+Model",
	EJS_SERVER_SEARCH: "http://www.compadre.org/osp/services/REST/search_v1_02.cfm?verb=Search&OSPType=EJSS+Model&Skip=0&Max=30&q=",	  
	
	EJS_COLLECTION_NAME: "EJS OSP Collection",
	EJS_INFO_URL: "http://www.compadre.org/OSP/online_help/EjsDL/DLModels.html",
	TRACKER_SERVER_TREE: "http://www.compadre.org/osp/services/REST/osp_tracker.cfm?verb=Identify&OSPType=Tracker",
	TRACKER_SERVER_RECORDS: "http://www.compadre.org/osp/services/REST/osp_tracker.cfm?OSPType=Tracker",
	TRACKER_COLLECTION_NAME: "Tracker OSP Collection",
	TRACKER_INFO_URL: "http://www.cabrillo.edu/~dbrown/tracker/library/comPADRE_collection.html",
	PRIMARY_ONLY: "&OSPPrimary=Subject",
	GENERIC_COLLECTION_NAME: "AAPT-ComPADRE OSP Collection",
	ABOUT_OSP: "About OSP and AAPT-ComPADRE",

	Description_DoubleClick: "dblClick",
	Description_Author: "Author",
	Description_DownloadSize: "Download Size", 
	Description_InfoField: "Info"
}

LibraryResource = {
   	UNKNOWN_TYPE: "Unknown",
   	COLLECTION_TYPE: "Collection",
   	TRACKER_TYPE: "Tracker",
   	EJS_TYPE: "EJS",
   	VIDEO_TYPE: "Video",
   	HTML_TYPE: "HTML",
   	PDF_TYPE: "PDF"
}

/* 
 * libraryResource
 */
libraryResource = function(parentid, name) {
	var self = {};
	var htmlPath=""; // rel or abs path to html that describes this resource
	var basePath=""; // base path for target and/or info
	var description=""; 
	var target=""; // rel or abs path to target 
	var type=LibraryResource.UNKNOWN_TYPE;
  	var properties = {};
    var thumbnail;
    var id = parentid + "/" + name;

  /**
   * Gets the id of its parent.
   *
   * @return the parent id
   */
	self.getParent = function() {
		return parentid;
	}
	
  /**
   * Gets the name of this resource (never null).
   *
   * @return the name
   */
	self.getName = function() {
		return name;
	}

  /**
   * Gets the id of this resource (never null).
   *
   * @return the id
   */
	self.getId = function() {
		return id;
	}
		
  /**
   * Gets the base path.
   *
   * @return the base path
   */
	self.getBasePath = function() {
		return basePath;
	}
	
  /**
   * Sets the base path of this resource.
   * 
   * @param path the base path
   * @return true if changed
   */
	self.setBasePath = function(path) {
		path = (!path)? "": path.trim(); 
		if (path != basePath) {
			basePath = path;
			return true;
		}
		return false;
	}
	
  /**
   * Gets the target of this resource (file name or comPADRE command).
   *
   * @return the target
   */
	self.getTarget = function() {
		return target;
	}

	function endsWith(str, suffix) {
    	return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}

  /**
   * Sets the target of this resource.
   * 
   * @param path the target path
   * @return true if changed
   */
	self.setTarget = function(path) {
		path = (!path)? "": path.trim(); 
		if (path != target) {
			thumbnail = null;
			target = path;
			path = path.toUpperCase();
			if (endsWith(path,".TRK")) 
				self.setType(LibraryResource.TRACKER_TYPE);
			else if (endsWith(path,".PDF")) 
				self.setType(LibraryResource.PDF_TYPE);
			else if (path.indexOf("EJS")>-1) { 
				self.setType(LibraryResource.EJS_TYPE);
			} else if (path == "") { 
				if (self.getHTMLPath() == null)
					self.setType(LibraryResource.UNKNOWN_TYPE);
				else
					self.setType(LibraryResource.HTML_TYPE);
			}
			return true;
		}
		return false;
	}
	
  /**
   * Gets the path to the html page displayed in the browser.
   *
   * @return the html path
   */
	self.getHTMLPath = function() {
		return htmlPath;
	}
	  	
  /**
   * Sets the html path of this resource.
   * 
   * @param path the html path
   * @return true if changed
   */
	self.setHTMLPath = function(path) {
		path = (!path)? "": path.trim();
		if (path != htmlPath) {
			htmlPath = path;
			if (!(self.addResource) // not collection 
					&& self.getTarget() == "") {
				if (path == "")	self.setType(LibraryResource.UNKNOWN_TYPE);
				else self.setType(LibraryResource.HTML_TYPE);
			}
			return true;
		}
		return false;
	}
	
  /**
   * Gets the description, which must be in html code.
   *
   * @return the description
   */
	self.getDescription = function() {
		return description;
	}
  	
  /**
   * Sets the description of this resource.
   * Note: the description must be in html code, since it is displayed
   * in the html pane of the LibraryTreePanel if the html path is empty.
   * 
   * @param desc the description in HTML code
   * @return true if changed
   */
	self.setDescription = function(desc) {
		desc = (!desc)? "": desc.trim();
		if (desc != description) {
			description = desc;
			return true;
		}
		return false;
	}
  	
  /**
   * Gets the type of resource.
   *
   * @return the one of the static constant types defined in this class
   */
	self.getType = function() {
		return type;
	}
  	
  /**
   * Sets the type of this resource.
   * The types are static constants defined in this class.
   * 
   * @param type the type
   * @return true if changed
   */
	self.setType = function(tp) {
		if (type != tp) {
			type = tp;
			return true;
		}
		return false;
	}
	
  /**
   * Sets an arbitrary property.
   * 
   * @param name the name of the property
   * @param value the value of the property
   */
	self.setProperty = function(name,value) {
		properties[name] = value;
	}
	
  /**
   * Gets a property value. May return null.
   * 
   * @param name the name of the property
   * @return the value of the property
   */
	self.getProperty = function(name) {
		return properties[name];
	}
			
  /**
   * Gets the thumbnail of this resource, if any.
   *
   * @return the thumbnail
   */
	self.getThumbnail = function() {
		return thumbnail;
	}

  /**
   * Gets the thumbnail of this resource, if any.
   *
   * @param imagePath the path to a thumbnail image
   */
	self.setThumbnail = function(imagePath) {
		thumbnail = imagePath;
	}

	/***
	 * To JSON
	 */	
	self.toJSON = function() {
		var json = {};
		json["id"] = id;
		json["parent"] = parentid;
		json["text"] = name;
		json["type"] = "file";		
		json["data"] = {};
		json["data"]["htmlPath"] = htmlPath;
		json["data"]["basePath"] = basePath;
		json["data"]["description"] = description; 
		json["data"]["target"] = target; 
		json["data"]["type"] = type;
		json["data"]["thumbnail"] = thumbnail;		
		return json;
	}

	return self;    
}

/* 
 * libraryCollection
 */
libraryCollection = function(parentid,name) {
	var self = libraryResource(parentid,name);	
  	var resources = {};      
    
  /**
   * Adds a resource to the end of this collection.
   *
   * @param resource the resource
   */
	self.addResource = function(resource) {
  		if (!resources[resource.getName()]) {
  			resources[resource.getName()] = resource;
  		}
  	}
    
  /**
   * Removes a resource from this collection.
   *
   * @param resource the resource to remove
   */
	self.removeResource = function(resource) {
  		if(resources[resource.getName()])
  			delete resources[resource.getName()];
  	}
  
  /**
   * Gets the array of resources in this collection.
   *
   * @return an array of resources
   */
	self.getResources = function() {
		return resources;
	}
	
	/***
	 * To JSON
	 */	
	self.toJSON = function() {
		var coljson = [];
		
		// collection root
		var json = {};
		json["id"] = self.getId();
		json["parent"] = self.getParent();
		json["text"] = self.getName();
		json["type"] = "folder";
		if(self.getName() === LibraryComPADRE.GENERIC_COLLECTION_NAME) 
			json["state"] = { opened: true, selected: true, disabled: false };			
		json["data"] = {};
		json["data"]["htmlPath"] = self.getHTMLPath();
		json["data"]["basePath"] = self.getBasePath();
		json["data"]["description"] = self.getDescription(); 
		json["data"]["target"] = self.getTarget(); 
		json["data"]["type"] = self.getType();
		json["data"]["thumbnail"] = self.getThumbnail();		
		coljson.push(json);
		
		// resources
		for(var i in resources) {
			var resource = resources[i];
			coljson = coljson.concat(resource.toJSON());			
		}
		
		return coljson;
	}
	
	self.setType(LibraryResource.COLLECTION_TYPE);
	return self;
}

/* 
 * libraryComPADRE
 */
libraryComPADRE = function() {
	var self = {};

	self.getNameFromId = function(id) {
		return id.substr(id.lastIndexOf("/")+1);
	}

	/**
	 * Loads a collection using a specified comPADRE search query.
	 * 
	 * @param collection the LibraryCollection to load
	 * @param query the search query
	 * @return true if successfully loaded
	 */
	self.load = function(collection, doc) {		
		var nodeList = doc.getElementsByTagName("Identify");
		var success = false;
		for (var i = 0; i < nodeList.length; i++) {
			success = loadSubtrees(collection, 
				nodeList.item(i).childNodes, "osp-subject", "") || success;
		}
		return success;
	}

	/**
	 * Loads a collection with subtree collections that meet the specified requirements.
	 * 
	 * @param collection the LibraryCollection to load
	 * @param nodeList a list of Nodes
	 * @param attributeType the desired attribute
	 * @param serviceParameter the desired service parameter
	 * @return true if at least one subtree collection was loaded
	 */
	function loadSubtrees(collection, nodeList, attributeType, serviceParameter) {
		var success = false;
		var dblClick = LibraryComPADRE.Description_DoubleClick;
		var parentId = collection.getId();
		for (var i = 0; i < nodeList.length; i++) {
			if (!(nodeList.item(i) instanceof Element))	continue;
			
			var node = nodeList.item(i);
			if (node.nodeName.toLowerCase() == "sub-tree-set" && attributeType == node.getAttribute("type")) { 
				var subTrees = getAllChildren(node, "sub-tree");
				if (subTrees.length > 0) { // node has subcategories
					var unclassifiedURL = null;
					for (var j = 0; j < subTrees.length; j++) {
						if (!(subTrees[j] instanceof Element))
							continue;
						var subtree =  subTrees[j];
						var name = subtree.getAttribute("name"); 
						var serviceParam = subtree.getAttribute("service-parameter"); 
						serviceParam = serviceParameter + "&" + getNonURIPath(serviceParam);
						if (name == "Unclassified") { // unclassified node is processed last and adds its records to the parent
							unclassifiedURL = serviceParam;
							continue;
						}
						var subCollection = libraryCollection(parentId,name);
						collection.addResource(subCollection);
						success = true;
						if (getAllChildren(subtree, "sub-tree-set").length == 0) { // has no subcategories
							var nodeName = "<h2>" + name + "</h2><blockquote>"; 
							subCollection.setDescription(nodeName + dblClick + "</blockquote>"); 
							subCollection.setTarget(serviceParam);
						} else
							loadSubtrees(subCollection,
									subtree.childNodes, attributeType + "-detail", serviceParam); 
					}
					if (unclassifiedURL != null) {
						collection.setTarget(unclassifiedURL);
					}
				}
			}
		}
		return success;
	}

	/**
	 * Get description for collection
	 * 
	 */
	self.getResources = function(collection, doc) {
		var success = false;
		var authorTitle = LibraryComPADRE.Description_Author; 
		var sizeTitle = LibraryComPADRE.Description_DownloadSize; 
		var infoFieldTitle = LibraryComPADRE.Description_InfoField; 

		// construct the complete tree path of the resource
		var parentList = ""; 
		var parentId = collection.getId();
		
		var list = doc.getElementsByTagName("record"); 
		for (var i = 0; i < list.length; i++) { // process nodes
			var node = list.item(i);
			// String ospType = getChildValue(node, "osp-type"); 
			var attachment = null;
			//if (ospType.startsWith("EJS")) { 
			//	attachment = getAttachment(node, "Source Code");       		
			//} else {
				attachment = getAttachment(node, "EJSS Model"); 
			//	if (attachment == null) {
			//		attachment = getAttachment(node, "Supplemental"); 
			//	}
			//}
			// ignore node if there is no associated attachment
			if (attachment == null)
				continue;
			// get the node data
			var name = getChildValue(node, "title"); 
			var record = libraryResource(parentId,name);
			collection.addResource(record);
			var downloadURL = processURL(attachment[0]);
			record.setTarget(downloadURL);
			record.setProperty("download_filename", attachment[1]); 
			var type = getChildValue(node, "osp-type").toUpperCase(); 
			type = type.indexOf("EJS") === 0 ? LibraryResource.EJS_TYPE : 
					(type == "TRACKER") ? LibraryResource.TRACKER_TYPE : LibraryResource.UNKNOWN_TYPE; 
			record.setType(type);
			var description = getChildValue(node, "description"); 
			var infoURL = getChildValue(node, "information-url"); 
			var thumbnailURL = getChildValue(node, "thumbnail-url"); 
			var authors = "";
			var children = getAllChildren(getFirstChild(node, "contributors"), "contributor"); 
			for (var j in children) {
				var el = children[j];  
				if ("Author" == (el.getAttribute("role")))
					authors += getNodeValue(el) + ", "; 
			}
			if (endsWith(authors,", ")) 
				authors = authors.substring(0, authors.length - 2);
			// assemble the html description
			var buffer = [];
			buffer.push("<p align=\"center\"><img src=\"" + thumbnailURL + "\" alt=\"" + name + "\"></p>"); 
			buffer.push("<p><b>" + parentList + "</b></p>");
			buffer.push("<h2>" + name + "</h2>");  
			if (authors.length > 0)
				buffer.push("<p><b>" + authorTitle + ":</b> " + authors + "</p>");
			if(description) buffer.push("<p>" + description.replace(/\n([ \t]*\n)+/g, "</p><p>").replace("\n", "<br />") + "</p>");
			buffer.push("<p><b>" + infoFieldTitle + "</b><br><a href=\"" + infoURL + "\">" + infoURL + "</a></p>");  
			buffer.push("<p><b>" + sizeTitle + "</b> " + attachment[2] + " bytes</p>");  
			record.setDescription(buffer.join(""));
			success = true;
		}

		return success;
	}

	/**
	 * Returns data for a downloadable DOM Node attachment.
	 * 
	 * @param node the DOM Node
	 * @param attachmentType the attachment type
	 * @return String[] {URL, filename, size in Bytes}, or null if no attachment found
	 */
	function getAttachment(node, attachmentType) {
		var id = getChildValue(node, "file-identifier"); 
		var childList = node.childNodes;
		var attachment = null;
		for (var i = 0, n = childList.length; i < n; i++) {
			var child = childList.item(i);
			if (!child.nodeName.toLowerCase() == "attached-document")continue; 
			var fileTypeNode = getFirstChild(child, "file-type"); 
			if (fileTypeNode != null
					&& attachmentType == getNodeValue(fileTypeNode)) {				
				var urlNode = getFirstChild(child, "download-url"); 
				if (urlNode != null) { // found downloadable attachment
					// keep first attachment or (preferred) attachment with the
					// same id as the node
					if (attachment == null
							|| id == getChildValue(child, "file-identifier")) { 
						var attachmentURL = getNodeValue(urlNode);
						var fileNode =  getFirstChild(child,
								"file-name"); 
						if (fileNode != null) {
							attachment = [attachmentURL,
									getNodeValue(fileNode),
									fileNode.getAttribute("file-size") ]; 
						} else
							attachment = [attachmentURL, null, null];
					}
				}
			}
		}
		return attachment;
	}

	/**
	 * Returns the first child node with the given name.
	 * 
	 * @param parent the parent Node
	 * @param name the child name
	 * @return the first child Node found, or null if none
	 */
	function getFirstChild(parent, name) {
		var childList = parent.childNodes;
		for (var i = 0, n = childList.length; i < n; i++) {
			var child = childList.item(i);
			if (child.nodeName.toLowerCase() == name.toLowerCase())
				return child;
		}
		return null;
	}

	/**
	 * Returns all child nodes with the given name.
	 * 
	 * @param parent the parent Node
	 * @param name the name
	 * @return a list of Nodes (may be empty)
	 */
	function getAllChildren(parent, name) {
		var list = [];
		var childrenList = parent.childNodes;
		for (var i = 0, n = childrenList.length; i < n; i++) {
			var child = childrenList.item(i);
			if (child.nodeName.toLowerCase() == name.toLowerCase())
				list.push(child);
		}
		return list;
	}

	/**
	 * Gets the value of a Node.
	 * 
	 * @param node the Node
	 * @return the value
	 */
	function getNodeValue(node) {
		for (var child = node.childNodes[0]; child != null; child = child
				.getNextSibling()) {
			if (child.nodeType == Node.TEXT_NODE)
				return child.nodeValue;
		}
		return null;
	}

	/**
	 * Gets the value of the first child node with a given name.
	 * 
	 * @param parent the parent Node
	 * @param name the name of the child
	 * @return the value of the first child found, or null if none
	 */
	function getChildValue(parent, name) {
		var node = getFirstChild(parent, name);
		if (node != null)
			return getNodeValue(node);
		return null;
	}

	/**
	 * Replaces "&amp" with "&" in HTML code.
	 * 
	 * @param url the HTML code
	 * @return the clean URL string
	 */
	function processURL(url) {
		var processed = [];
		var index = url.indexOf("&amp;"); 
		while (index >= 0) {
			processed.push(url.subSequence(0, index + 1));
			url = url.substring(index + 5);
			index = url.indexOf("&amp;"); 
		}
		processed.push(url);
		return processed.toString();
	}

	/**
	 * Returns a descriptive name for a given ComPADRE path (query).
	 * 
	 * @param path the query string
	 * @return the name of the collection
	 */
	function getCollectionName(path) {
		if (path.indexOf(LibraryComPADRE.EJS_SERVER_TREE) === 0)
			return LibraryComPADRE.EJS_COLLECTION_NAME;
		if (path.indexOf(LibraryComPADRE.TRACKER_SERVER_TREE) === 0)
			return LibraryComPADRE.TRACKER_COLLECTION_NAME;
		return LibraryComPADRE.GENERIC_COLLECTION_NAME;
	}

	/**
	 * Returns the LibraryCollection for a given ComPADRE path (query).
	 * 
	 * @param path the query string
	 * @return the collection
	 */
	self.getCollection = function(parent, path, doc) {
		var name = getCollectionName(path);
		var primarySubjectOnly = path.indexOf(LibraryComPADRE.PRIMARY_ONLY) > -1;
		var collection = libraryCollection(parent, name);
		if (name == LibraryComPADRE.EJS_COLLECTION_NAME) {
			collection.setHTMLPath(LibraryComPADRE.EJS_INFO_URL);
		} else if (name == LibraryComPADRE.TRACKER_COLLECTION_NAME) {
			collection.setHTMLPath(LibraryComPADRE.TRACKER_INFO_URL);
		}
		var aboutOSP = libraryResource(collection.getId(),LibraryComPADRE.ABOUT_OSP);
		aboutOSP.setHTMLPath(LibraryComPADRE.OSP_INFO_URL);
		collection.addResource(aboutOSP);
		
		self.load(collection, doc);
		var base = LibraryComPADRE.EJS_SERVER_RECORDS;
		if (name == LibraryComPADRE.TRACKER_COLLECTION_NAME) {
			base = LibraryComPADRE.TRACKER_SERVER_RECORDS;
		}
		if (primarySubjectOnly)
			base += LibraryComPADRE.PRIMARY_ONLY;
		collection.setBasePath(base);
		return collection;
	}

	/**
	 * Returns the collection path for an EJS or tracker tree.
	 * 
	 * @param path the ComPADRE query string
	 * @param primarySubjectOnly true to limit results to their primary subject
	 * @return the corrected ComPADRE query string
	 */
	function getCollectionPath(path, primarySubjectOnly) {
		var isPrimary = path.endsWith(PRIMARY_ONLY);
		if (isPrimary && primarySubjectOnly)
			return path;
		if (!isPrimary && !primarySubjectOnly)
			return path;
		if (!isPrimary && primarySubjectOnly)
			return path + PRIMARY_ONLY;
		return path.substring(0, path.length() - PRIMARY_ONLY.length());
	}

	function endsWith(str, suffix) {
    	return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}

	/**
	 * Determines if a query path limits results to the primary subject only.
	 * 
	 * @param path the path
	 * @return true if path contains a primary-subject-only flag
	 */
	function isPrimarySubjectOnly(path) {
		return path.indexOf(PRIMARY_ONLY) > -1;
	}

	function getNonURIPath(uriPath) {
		if (uriPath == null)
			return null;
		var path = uriPath;
		// String path = XML.forwardSlash(uriPath.trim());
		// remove file protocol, if any
		if (path.indexOf("file:") === 0) { 
			path = path.substring(5);
		}
		// remove all but one leading slash
		while (path.indexOf("//") === 0) { 
			path = path.substring(1);
		}
		// remove last leading slash if drive is specified
		if (path.indexOf("/") === 0 && path.indexOf(":") > -1) {
			path = path.substring(1);
		}
		// replace "%20" with space
		var j = path.indexOf("%20"); 
		while (j > -1) {
			var s = path.substring(0, j);
			path = s + " " + path.substring(j + 3); 
			j = path.indexOf("%20"); 
		}
		// // replace "%26" with "&"
		//	    j = path.indexOf("%26");                           
		// while(j>-1) {
		// String s = path.substring(0, j);
		//	      path = s+"&"+path.substring(j+3);                
		//	      j = path.indexOf("%26");                         
		// }
		return path;
	}

	return self;
}();

/*
 * Manage the download of the library from url
 */
cloudComPADREConnection = function(suburl, selectedNode, isroot, callback_ok, callback_progess, callback_error) {
	if(isroot) {
	    // var url = LibraryComPADRE.EJS_SERVER_TREE + LibraryComPADRE.PRIMARY_ONLY;
	    var url = LibraryComPADRE.EJS_SERVER_TREE_LOCAL + LibraryComPADRE.PRIMARY_ONLY;			
	} else {
		// var url = LibraryComPADRE.EJS_SERVER_RECORDS;
		var url = LibraryComPADRE.EJS_SERVER_RECORDS_LOCAL;
		url += suburl;		
	}
	var nodeid = selectedNode?selectedNode.id:"#";

    // connection
    var query = url.split("?");
	var http = new XMLHttpRequest();
	http.onprogress = callback_progess;
	http.responseType = "document";
	http.open("POST", query[0], true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	http.onreadystatechange = function() { // call a function when the state changes.
	    if(http.readyState == 4) {
	    	if (http.status == 200 && http.responseXML && http.responseXML.documentElement) {
	    		// add elements into treeBuilder
	    		var doc = http.responseXML.documentElement;
		    	if(isroot) { // get collections
		    		// add elements into treeBuilder
			    	var library = libraryComPADRE.getCollection(nodeid, url, doc);	
					callback_ok(library);		    		
		    	} else { // get resources
			    	// add elements into treeBuilder
			    	var nodeName = libraryComPADRE.getNameFromId(nodeid);
			    	var collection = libraryCollection(selectedNode.parent,nodeName);
			    	if(libraryComPADRE.getResources(collection, doc)) {
			    		callback_ok(collection);
			    	}
		    	}
	    	} else {
	    		console.log("No XML content in OSP connection!");
	    		callback_error();
	    	}	        
	    }
	}
	http.send(query[1]);
}    


