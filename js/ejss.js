/** 
 * Copyright 2015 - Félix J. García
 * 
 * @author Félix J. García (fgarcia@um.es)
 */

// constantes
var ROOTFOLDERNAME = "models";
var CONTENTFILE = "/models/content.json";
var BASEFOLDER  = "models/";
var IMAGESFOLDER = "images/"
var PHOTOSFOLDER = "photos/"
var METADATAFILE = "_metadata.txt";

var PACKAGE_KEY = "package";
var PAGE_KEY = "page-index";
var TITLE_KEY = "page-title";
var MODEL_KEY = "model";
var AUTHORIMG_KEY = "author-image";
var LOGO_KEY = "logo-image";
var FOLDER_KEY = "folder";
var AUTHOR_KEY = "author";
var NAME_KEY = "title";
var COPYRIGHT_KEY = "copyright";
var READONLY_KEY = "read-only";
var MAINSIM_KEY = "main-simulation";
var HTMLMAIN_KEY = "html-main";
var INTERNAL_KEY = "internalmap"; 

var mRootFolder = [];	// items del folder raiz
var mFolders = [];		// listado de folders
var mFolder = [];   	// items del folder base
var mFoldername;         // nombre del folder base
var mFolderuri;          // uri del folder base

// modelos iniciales precargados
var premodels = ["initial/ejss_model_05_FerrisWheelJS.zip","initial/ejss_model_08_FlatMirror.zip"];

function initModelManager(callback) {
	mFolderuri = ROOTFOLDERNAME;
	
	// comprobar si existen directorios y content.json
	filer.exist(CONTENTFILE, function(file) {
		console.log("Filesystem already exists!");
		findModels(callback);
	}, function(e) {
		// console.log("Creating filesystem!");
		// crea estructura básica interna del filesystem
		filer.mkdir(BASEFOLDER, false, function(dirEntry) {
			filer.mkdir(IMAGESFOLDER, false, function(dirEntry) {
				filer.mkdir(PHOTOSFOLDER, false, function(dirEntry) {
					mFolders.push({ "name": "models", "uri": "models", "items": [] });				
					saveChanges( function() { findModels(callback); } );
					
					// añade modelos por defecto
					addItemFromHttpFile(premodels[0], function() { 
						addItemFromHttpFile(premodels[1], function() { 
							findModels(callback); 
						} );
					} );
					
				}, onError);
			}, onError);
		}, onError);		
	});	
}

function restartModelManager(callback) {	
	// comprobar si existen directorios y content.json
	filer.rm(CONTENTFILE, function(file) {
		filer.rm(BASEFOLDER, function(file) {
			filer.rm(IMAGESFOLDER, function(file) {
				filer.rm(PHOTOSFOLDER, function(file) {
					// console.log("Filesystem deleted!");
					mRootFolder = [];
					mFolders = [];
					mFolder = [];
					initModelManager(callback);
				});
			});
		});
	}, onError);	
}

function backupMemory(callback) {
	// https://gildas-lormeau.github.io/zip.js/demos/demo1.html
}

function changeFolder(uri, callback) {
	mFolderuri = uri;
	findModels(callback);
}

function reload(callback) {
	findModels(callback);
}

function findModels(callback) {       
    // leo contenido del fichero con la estructura de folders (content.json)
    filer.open(CONTENTFILE, function(file) {
		var reader = new FileReader();
		reader.onload = function(e) {
			mFolders = JSON.parse(e.target.result);
			
			// obtengo los folders	
			for(var i=0; i<mFolders.length; i++) {
		        var uri = mFolders[i]["uri"];;
		        
		        // obtengo el folder que se busca
		        if(uri == mFolderuri) {
		            mFolder = mFolders[i]["items"];
		            mFoldername = mFolders[i]["name"];
		        }
		        
		        // obtengo el folder raiz
		        if(uri == ROOTFOLDERNAME) {
		            mRootFolder = mFolders[i]["items"];
		        }	        
			}   	
			
			// callback
			if (callback) callback();			
		};
		reader.readAsText(file);    	 
	}, onError);
}

function saveChanges(callback) {
    // creo el string con el json
    var json = "[";
    for(var i=0; i<mFolders.length-1; i++)
    	json += JSON.stringify(mFolders[i]) + "," ;
    json += JSON.stringify(mFolders[i]) + "]"
    
    // copio string al fichero con la estructura de folders (content.json)
	filer.write(CONTENTFILE, {data: json, type: 'text/plain'}, function(fileEntry, fileWriter) {
		if(callback) callback();
	}, onError);
}

function delItem(index, callback) {
	var item = mFolder[index];
	var type = item["type"];
    
    // borra elemento
    if(type == 0) { // model
    	mFolder.splice(index,1);
        
        // borra memoria 		
		filer.rm(BASEFOLDER + item.uri + ".zip", function(){}); // zip			          			
		filer.rm(BASEFOLDER + item.uri, function(){}); // directorio
		filer.rm(IMAGESFOLDER + item.uri + ".png", function(){}) // directorio logo
		if(item.author) {
			var count = item.author.split(';').length;
			for(var i=0; i<count; i++)
				filer.rm(PHOTOSFOLDER + item.uri + i + ".png", function(){}); // directorio photos
		}					        

    } else if (type == 1) { // folder
        // borra el folder
        var uri = item["uri"];
        var ix = getFolderIndexForUri(uri);
        if(ix != -1) {
        	mFolders.splice(ix,1);
        }
        
        // borra el elemento
    	mFolder.splice(index,1);
        
        // borra memoria 		
		filer.rm(BASEFOLDER + item.uri + ".zip", function(){}); // zip			          			
		filer.rm(BASEFOLDER + item.uri, function(){}); // directorio
		filer.rm(IMAGESFOLDER + item.uri, function(){}) // directorio logo
		filer.rm(PHOTOSFOLDER + item.uri, function(){}) // directorio photos					        
		filer.rm(IMAGESFOLDER + item.uri + ".png", function(){}) // directorio logo
		if(item.author) {
			var count = item.author.split(';').length;
			for(var i=0; i<count; i++)
				filer.rm(PHOTOSFOLDER + item.uri + i + ".png", function(){}); // directorio photos
		}					        
    }
    
    // guardo los cambios
    saveChanges(callback);    
}

function addFolder(name, callback) {
    // uri para nuevo item
    var uristr = randomURI(25);
    
    // añado el nuevo folder al listado dentro del folder actual
    var item = {
    	uri: uristr,
    	name: name,
    	type: 1,
    	author: "",
    	readonly: false    	
    }
    mFolder.splice(0,0,item);
    
    // añado el folder al listado de folders
    var folder = {
    	uri: uristr,
    	name: name,
    	items: []
    }
    mFolders.push(folder);
    
    // guardo los cambios
    saveChanges(callback);	    
}

function changeNameFolder(item, name, callback) {
    // cambio nombre del item
    item["name"] = name;

    // cambio nombre en folder
    var uri = item["uri"];
    var ix = getFolderIndexForUri(uri);
    mFolders[ix]["name"] = name;
    
    // guardo los cambios
    saveChanges(callback);    
}

function moveItem(srcix, toix, callback) {
	if (srcix != toix) {
		var srcitem = mFolder[srcix];
		var toitem = mFolder[toix];
		
		var type = toitem["type"];
	    if(type == 0) { // to model
	    	// change position
	    	mFolder.splice(srcix, 1);
			mFolder.splice(toix, 0, srcitem);
	    	
		} else if (type == 1) { // to folder
    		var folderix = getFolderIndexForUri(toitem["uri"]);
    		if(folderix != -1) {
        		// añado item al folder
        		mFolders[folderix]["items"].splice(0,0,srcitem);			
        
		        // elimino item del actual folder
		        mFolder.splice(srcix,1);
		    }			
		}

    	// guardo los cambios
    	saveChanges(callback);           
	}
}

function moveItemUsingUri(srcix, toUri, callback) {
	var srcitem = mFolder[srcix];
	var folderix = getFolderIndexForUri(toUri);
	if(folderix != -1) {
		// añado item al folder
		mFolders[folderix]["items"].splice(0,0,srcitem);			

        // elimino item del actual folder
        mFolder.splice(srcix,1);
        
		// guardo los cambios
		saveChanges(callback);                   
    }			
}


function getFolderIndexForUri(uri) {
	for(var i=0; i<mFolders.length; i++) {
		var folder = mFolders[i];
		
        // obtengo el folder que se busca por uri
        var uristr = folder["uri"];
        if(uristr == uri) {
            return i;
        }        
    }
    return -1;
}

// lee los metadata del modelo y de los modelos internos
function readAllMetadata(uri,callback) {
	var dirbase = BASEFOLDER + uri;
	
    // lee metadata principal
    readMetadata(dirbase, 0, function(map) {
	    map[INTERNAL_KEY] = [];
	    
	    // cuento modelos internos
	    var count = 0;	    
	    var internalDir = map[MODEL_KEY];
	    while(internalDir) {	        		        
	        var modelkey = MODEL_KEY + count;
	        internalDir = map[modelkey];
	        count++;	        	
	    }    	
	    
	    // obtengo los metadatos internos en base a la clave model
	    if(count) {    
		    var index = 0;
		    var internalDir = map[MODEL_KEY];
		    while(internalDir) {	        
		        var inter = readMetadata(dirbase, internalDir, function(internalDir, inter) {
			        // inserto los metadatos internos junto con el resto de metadatos
			        map[INTERNAL_KEY][internalDir] = inter;
			        
			        // si es el último, invoco callback
			        count--;
			        if ((count == 0) && callback) callback(map);
		        });	        
			        
		        var modelkey = MODEL_KEY + index;
		        internalDir = map[modelkey];
		        index++;	        	
		    }
		} else {
			// invoco callback
			if (callback) callback(map);
		}    		    	    
    });
}

// lee los metadata del modelo
function readMetadata(dirbase, internalDir, callback) {	
	var uri = internalDir? dirbase + "/" + internalDir: dirbase;
	var map = [];
	
    // ruta al fichero metadata 
    var fileRoot = uri + "/" + METADATAFILE;

    // lee todo el texto
  	filer.open(fileRoot, function(file) {
        var reader = new FileReader();
        reader.onload = function(e) {        	
			// separa el texto por líneas
			var lines = e.target.result.split('\n');

			// obtiene las líneas y las procesa
			var pageIndex = 0;
			var pageTitle = 0;
			var modelInside = 0;
          
          	lines.forEach(function(line, i) {

		        // separar en cadenas la línea en base al separador :
		        var singleStrs = line.split(':');

		        // si no tiene al menos dos elementos, lo ignoramos
		        if(singleStrs.length >= 2) {
		            var key = singleStrs[0].trim();
		            var value = line.substring(line.indexOf(':') + 1).trim();
		
		            // quita posibles comentarios de value |
		            value = value.split('|')[0];
		            
		            // compruebo si la clave existe para cambiarla
		            if(map[key]) {
		            	if(key == PAGE_KEY) {
		            		key = key + pageIndex;
		            		pageIndex++;
		            	} else if(key == TITLE_KEY) {
		            		key = key + pageTitle;
		            		pageTitle++;
		            	} else if(key == MODEL_KEY) {
		            		key = key + modelInside;
		            		modelInside++;
		            	}
		            }
					
		            // inserto en el mapa
		            map[key] = value.trim();
		        }
			});
			
			// callback
			if (callback) internalDir?callback(internalDir, map):callback(map);
        };
        reader.readAsText(file);				          	    	  		
  	}, onError);
}

function unzip(file, dirbase, callback) {
	filer.mkdir(dirbase, false, function(dirEntry) {
		
		// descomprimo el fichero				
		zip.createReader(file, function(zipReader) {
		  	
		  	// optengo las entradas del fichero comprimo
			zipReader.getEntries(function(zipentries) {
				var wcount = 0;
		
				// copio cada fichero descomprimido en el directorio temporal				
				zipentries.forEach(function(fzip, count) {
					// console.log(fzip.filename);		
						
					// obtengo directorio/s donde situar el fichero descomprimido
			      	var folders = (dirbase + "/" + fzip.filename).split('/');
			      	folders = folders.slice(0, folders.length - 1);
			
			      	// creo directorio/s antes de copiar el fichero
			      	filer.mkdir(folders.join('/'), false, function(dirEntry) {			      		
			        	var path = dirbase + "/" + fzip.filename;

					    // console.log("<---" + fzip.filename);
		
			        	// escribo cada fichero a su path (excepto directorios, incluido /.)
			        	if ((path.lastIndexOf('/') !=  path.length - 1) && (path.lastIndexOf('/.') !=  path.length - 2)) {
			          		fzip.getData(new zip.BlobWriter(), function(blob) {
			          			filer.write(path, {data: blob}, function(fileEntry,fileWriter) {
   									// invoca al callback después de leer todos los ficheros
			        				if(wcount == zipentries.length-1) callback();
			        				wcount++;
			        				console.log("file " + wcount + " of " + zipentries.length);
			          			}, onError);
			            	});					
			          	} else { // el último elemento coincide con el directorio :-O
			          		if(wcount == zipentries.length-1) callback();
			          		wcount++;
			          	}
					}, onError);
				});
			});								
		});								
	}, onError);
	
}

function addItemFromBlobFile(file, callback) {
	addItemFromUri(new zip.BlobReader(file), callback);
}

function addItemFromHttpFile(file, callback) {
	addItemFromUri(new zip.HttpReader(file), callback);
}

function addItemFromUri(file, callback) {
	// uri y filename para nuevo item
	var uristr = randomURI(25);		
	
	// creo directorio propio para el modelo y descomprimo ahí el modelo
	var dirbase = BASEFOLDER + uristr;
	unzip(file, dirbase, function() {			          			
		// copio zip también
		file.readUint8Array(0, file.size, function(arraybuffer) {
			filer.write(dirbase + ".zip", {data: arraybuffer});			          						
		});
		
    	// console.log("read metadata");
        readAllMetadata(uristr, function(map) {
	        // compruebo si es un folder
	        //if(map[PACKAGE_KEY]) {
	        if(map[FOLDER_KEY]) {
	            // crea un nuevo folder
	            var roaccess = map[READONLY_KEY];
	
	            // añado el nuevo folder al listado
	            var item = newFromMap(map,uristr,1,roaccess);
	            mFolder.splice(0,0,item);
	            
	            // creo nuevo folder
	            var folder = { uri: uristr, name: item["name"] };

				// creo directorios y añado ficheros internos
                var logodir = IMAGESFOLDER + uristr;
                var photodir = PHOTOSFOLDER + uristr;
				filer.mkdir(logodir, false, function(dirEntry) {
					filer.mkdir(photodir, false, function(dirEntry) {						
		                copyModel(map, 0, uristr, function() { saveChanges(callback) });		                						
					}, onError);
				}, onError);

				// añado elementos al folder
	            var items = [];
	            for(var key in map[INTERNAL_KEY]) {
	            	var imap = map[INTERNAL_KEY][key];	            	
	                var iuristr = uristr + "/" + key;
				
	                // añade el modelo 
	                var item = newFromMap(imap,iuristr,0,false);
	                items.push(item);
				}				
	            folder["items"] = items;
	            mFolders.push(folder);
		        
	        } else { // se trata de un modelo
	            // añado el nuevo item al listado
	            var item = newFromMap(map,uristr,0,false);
	            mFolder.splice(0,0,item);

		        // copia datos a memoria del modelo
		        copyModel(map,-1,uristr,function() {saveChanges(callback) });		
	        }	
        });        
	}, onError);
}

function newFromMap(map, uristr, type, roaccess) {
    var item = {};
    if(map[NAME_KEY]) {
        item["name"] = map[NAME_KEY];
    } else if(map[FOLDER_KEY]) {
    	item["name"] = map[FOLDER_KEY];
    } else {
    	item["name"] = "No title";
    }
    
    item["author"] = map[AUTHOR_KEY];
    item["uri"] = uristr;
    item["type"] = type;
    item["copyright"] = map[COPYRIGHT_KEY];
    item["readonly"] = roaccess;
    item["authorimg"] = map[AUTHORIMG_KEY];
    
    return item;
}

function copyModel(map, pos, uristr, callback) {

    function copyFiles(imap, srcdir, destdir, name, cb) {
		// console.log("entra ", imap, srcdir, destdir, name);
		var dirbase = BASEFOLDER + srcdir;
			
		// copia una foto
		function copyPhoto(photos, i, photodest) {
	        var photo = photos[i];
			var photofile = dirbase + "/" + photo;
			// console.log("cp photo ", photofile, photodest, name + i + ".png");
			
			filer.cp(photofile, photodest,name + i + ".png",
				function() {
					i = i + 1;
					if(i<photos.length) {
						// copia la siguiente foto a memoria
						copyPhoto(photos,i,photodest);
					} else // si es la última foto, sigo con el proceso de copia
						copyRestFiles();								
				},
				// si hay un error, sigo con el proceso 
				copyRestFiles);		
		}
		
		// copia siguiente modelo
		function copyRestFiles() {
			if(cb) cb(); // solo existirá al copiar el folder
			else {
				pos = pos + 1;
				if(pos<keys.length) {
					// copia datos a memoria del siguiente modelo interno
					copyModel(map,pos,uristr,callback);
				} else {
			        // copia datos a memoria del folder
			        copyFiles(map,uristr,".",uristr,callback);		
				}
			}						
		}
		
		// copia fotos
		function copyPhotos() {
			// copia las fotos a memoria
		    var photouri = imap[AUTHORIMG_KEY];
		    if(photouri) {
		        var photos = photouri.split(';');
				var photodest = PHOTOSFOLDER + destdir;
		       	copyPhoto(photos, 0, photodest);
		    } else // puede que no haya fotos, sigo con el proceso de copia
		    	copyRestFiles();
		    				
		}
			
	    var logouri = imap[LOGO_KEY];
	    if(logouri) {
	    	var logodest = IMAGESFOLDER + destdir;
	    	var logofile = dirbase + "/" + logouri;
			// console.log("cp logo ", logofile, logodest, name);
	    	
	    	filer.cp(logofile,logodest,name + ".png", copyPhotos,
				// si hay un error, sigo con el proceso 
				copyPhotos); 
	    } else // puede que no tenga logo, sigo con el proceso de copia 
	    	copyPhotos();
	}
	
	if(pos == -1) {	// se trata de un modelo simple
		copyFiles(map,uristr,".",uristr,callback);
	} else { // se trata de un folder
		var keys = Object.keys(map[INTERNAL_KEY]);  // keys
		if(keys.length == 0) return;
		
		var name = keys[pos];			             // key actual 
		var imap = map[INTERNAL_KEY][name];	     // imap actual		            	
	    var srcdir = uristr + "/" + name;			 // directorio origen
	    var destdir = uristr;						 // directorio destino
		
		copyFiles(imap, srcdir, destdir, name);	
	}	
}

function randomURI(len) {
    var text = " ";

    var charset = "abcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < len; i++ )
        text += charset.charAt(Math.floor(Math.random() * charset.length));

    return text.trim();
}