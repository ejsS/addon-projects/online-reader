/** 
 * Copyright 2015 - Félix J. García
 * 
 * @author Félix J. García (fgarcia@um.es)
 */

var filer = new Filer();

var comPADREweb = "http://www.compadre.org/osp/EJSS/";

var SIMHTML = [
	'<!doctype html>',
	'<html lang="en">',
	'<head>',
	'  <meta charset="utf-8">',
	'  <title>EjsS Reader - %title%</title>',
	'  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">',
	'  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>',
	'  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>',
	'  <script>',
	'  $(function() {',
	'    $(document).ready(function(){',
	'      $("#menu").menu({position: { my: "right top", at: "right bottom"},',
	'	     select: function(event, ui) {',
	'	       if(ui.item.children().attr("tag")) $("#frame").attr("src", ui.item.children().attr("tag"));',
	'      	   menu.menu( "collapseAll", null, true );',
	'	     }',
	'      });',
	'    });',
	'  });',
	'  </script>',
	'  <style>.ui-menu { width: 150px; }</style>',
	'</head><body>',
	'<iframe id="frame" style="border: 0; position:fixed; top:0; left:0; right:0; bottom:0; width:100%; height:100%" src="%sim%"></iframe>',
	'<ul id="menu" style="float: right; margin: 0px 10px">',
  	'  <li ><span class="ui-icon ui-icon-document"></span>Pages<ul>%pages%</ul></li>',
	'</ul>',
	'</body></html>'
]

var SIMHTML2 = [
	'<!doctype html>',
	'<html lang="en">',
	'<head>',
	'  <meta charset="utf-8">',
	'  <meta http-equiv="cache-control" content="max-age=0" />',
	'  <meta http-equiv="cache-control" content="no-cache" />',
	'  <meta http-equiv="expires" content="0" />',
	'  <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />',
	'  <meta http-equiv="pragma" content="no-cache" />	',
	'  <title>EjsS Reader - %title%</title>',
	'  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">',
	'  <link rel="stylesheet" href="css/model.css" type="text/css">',
	'  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>',
	'  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>',
	'  <script>',
	'  function changeSrc(element) {',
	'		$("#frame").attr("src", element.attr("tag"));',
	'       var pos = element.attr("pos");',
	'       if(pos == "first") ',
	'	    	$("button[back]").prop( "disabled", true );',			
	'	    else',	
	'	       	$("button[back]").prop( "disabled", false );',			
	'       if(pos == "last") ',
	'	       	$("button[forward]").prop( "disabled", true );',			
	'	    else',	
	'	       	$("button[forward]").prop( "disabled", false );',
	'       $("#toolbar").attr("pos",pos);',				
	'  }',
	'  $(function() {',
	'    $(document).ready(function(){',
	'      $("#menu").menu({position: { my: "right top", at: "right bottom"},',
	'	     select: function(event, ui) {',
	'	       if(ui.item.children().attr("tag")) {',
	'      	      1|$("#menu").menu( "collapse");',
	'      	      1|$("#menu").menu( "collapseAll", null, true );',
	'			  changeSrc(ui.item.children());',
	'	       }',	
	'	     }',
	'      });',
	'      1|$( "#frame" ).mouseenter(function() {',
	'      	    1|$("#menu").menu( "collapse");',
	'      	    1|$("#menu").menu( "collapseAll", null, true );',
	'	   });',
	'	   $("button[back]").prop( "disabled", true );',		
	'	   $("button[back]").click( function() {',
	'          var path = "a[pos=\'" + $("#toolbar").attr("pos") + "\']";',
	'		   changeSrc($(path).parent().prev().children());',
	'      });',			
	'	   $("button[forward]").click( function() {',
	'          var path = "a[pos=\'" + $("#toolbar").attr("pos") + "\']";',
	'		   changeSrc($(path).parent().next().children());',
	'      });',			
	'    });',
	'  });',
	'  </script>',
	'</head><body>',
	'<iframe id="frame" style="z-index: -100; border: 0; position:fixed; top:0; left:0; right:0; bottom:0; width:100%; height:100%" src="%sim%"></iframe>',
	'<div id="toolbar" pos="first">',
	'  <button back></button>',
	'  <button forward></button>',
	'    <ul id="menu">',
  	'      <li><ul>%pages%</ul></li>',
	'    </ul>',
	'</div>',
	'</body></html>'
]

var VIEWABLE_FILES = [
  '.zip'
];

var TICKER_LIST = [
  'Drag & Drop files from your desktop to your browser.',
  'Drag & Drop files and folders to easily organize the reader view.',
  'ESC gets you out of modal windows.',
  'Download <a target="_blank" href="http://www.um.es/fem/EjsWiki/Main/ReaderApp">EjsS Reader</a> for Android and iOS.',
  'Easily create your own models using <a target="_blank" href="http://www.um.es/fem/EjsWiki/pmwiki.php">EjsS Authoring Tool<a>.',
  'If you find any bug, please report <a href="mailto:fgarcia@um.es?subject=[Chrome Reader] bug&body=Hi Felix, I found a bug! It is ...">here</a>.'
];

var DIGITAL_LIBRARY = [
  ['OSP collection in the AAPT-ComPADRE digital library','osp.html'],
  ['EjsS models at Davidson College','http://ejs.davidson.edu'],
  ['EjsS models at Eckerd College','http://academics.eckerd.edu/physics/EJS'],
  ['EjsS models at Universidad de Murcia','http://fem.um.es/EjsDL/Examples'],
  ['EjsS models at Singapore','http://iwant2study.org/lookangejss/']
];

// Cache some frequently used DOM elements.
var librariesNav = document.querySelector('#libraries-nav');
var filePreview = document.querySelector('#file-info');
var pathContainer = document.querySelector('#path');
var filesContainer = document.querySelector('#files');
var fileList = filesContainer.querySelector('ul');
var errors = document.querySelector('#errors');
var backupButton = document.querySelector('#backup');
var gridButton = document.querySelector('#grid');
var restartButton = document.querySelector('#restart');
var libraryButton = document.querySelector('#library');
var importButton = document.querySelector('#import');
var importFileInput = document.querySelector('#myfile');
var createButton = document.querySelector('#createButton');
var ticker = document.querySelector('#ticker');
var viewmodel = document.querySelector('#viewmodel');
var uriParam = location.search.split('uri=')[1];

var readonly = false; // identifica el folder actual como readonly
var path = [];
	
function openFS() {
  // muestro header y footer
  document.querySelector('.offscreen').classList.remove('offscreen');

  try {
    filer.init({persistent: true, size: 250 * 1024 * 1024}, function(fs) {
      console.log(fs.root.toURL());
      console.log('Opened: ' + fs.name);

	  // si se indica una uri, se abre esa uri
  	  if(uriParam) {
  	  	  // abre simulación de la uri
		  openFile(uriParam);
	  } else {
	      // inicializa manager
		  initModelManager(function() { 
		  	renderEntries();		  	
		  });

		  // texto con info de uso	
	      showUsage();
	  }
    }, function(e) {
      if (e.name == 'SECURITY_ERR') {
        errors.textContent = 'SECURITY_ERR: Are you running in incognito mode?';
        return;
      }
      onError(e);
    });
  } catch(e) {
    if (e.code == FileError.BROWSER_NOT_SUPPORTED) {
      fileList.innerHTML = 'BROWSER_NOT_SUPPORTED';
    }
  }
}

function refreshFolder() {
  errors.textContent = ''; // reset errors
  
  // recargo folder
  reload(function() { renderEntries(); });
     
}

function showUsage() {
  filer.df(function(byteUsed, byteFree, byteCap) {
  	console.log("Used: " + byteUsed + ", Free: " + byteFree + ", Cap: " + byteCap);
  }, onError);
}

function onError(e) {
  if(e.name) {
  	console.log('Error: ' + e.name + '\n');
  	errors.textContent = e.name;
  } else {
  	console.log('Error: ' + e + '\n');
  	errors.textContent = e;  	
  }
}

function renderEntries() {  
  // activo o desactivo botones de interface
  if (readonly) {
  	importButton.disabled = true;
  	createButton.disabled = true;
  	libraryButton.disabled = true;
  } else {
  	importButton.disabled = false;
  	createButton.disabled = false;
  	libraryButton.disabled = false;  				
  } 
  
  // inicializo lista de entradas
  fileList.innerHTML = ''; 
  pathContainer.innerHTML = '';
  
  // referencia a los directorios del path  
  var li = document.createElement('li');
  var html = [];
  html.push('<a uri="' + ROOTFOLDERNAME + '" href="javascript:" onclick="loadFolderFromPath(\'\')" class="pathFolder">' + 
      '<img uri="' + ROOTFOLDERNAME + '" src="images/icons/folder.png" class="pathFolder" onclick="loadFolderFromPath(\'\')">' + 
      ' [ root ] \/</a>');
  for(var i=0; i<path.length; i++) {
  	var item = path[i];
  	var readonly = item.readonly && (item.readonly == "true");
  	html.push('<a uri="' + item.uri + '" href="javascript:" onclick="loadFolderFromPath(\'' + item.uri + '\',' + i + ',' + readonly + ')" class="pathFolder">' + 
		  '<img uri="' + item.uri + '" src="images/icons/folder.png" class="pathFolder" onclick="loadFolderFromPath(\'' + item.uri + '\',' + i + ',' + readonly + ')">' + 
	      ' [ ' + item.name + ' ] \/</a>');  	
  }
  li.innerHTML = html.join('');
  pathContainer.appendChild(li);
  
  // en caso de no existir ficheros
  if (!mFolder.length) {
    var li = document.createElement('li');
    li.innerHTML = 'No files. Import some!'
    fileList.appendChild(li);
    return;
  }

  // añado entradas
  for(var i=0; i<mFolder.length; i++) {
  	var li = document.createElement('li');
  	li.innerHTML = constructEntryHTML(mFolder[i],i);  	
  	fileList.appendChild(li);
  }
}

function constructEntryHTML(item,index) {  
  var roentry = item.readonly && (item.readonly == "true");
  var html = [];
    
  // image
  if(item.type == 1) {
  	  html.push('<div data-img class="folder">');
	  html.push('<img ix="' + index + '" src="' + filer.fs.root.toURL() + IMAGESFOLDER + item.uri + ".png" + '" class="logo" onclick="loadFolder(' + index + ')" title="Open folder" alt="Open folder"' +
	   ' onError="this.onerror=null;this.src=\'images/icons/folder.png\';">');  	  	
      html.push('</div>');
  } else {
  	  html.push('<div data-img class="file">');
      // html.push('<a href="index.html?uri=' + mFolder[index].uri + '" target="viewmodel">');
      html.push('<img ix="' + index + '" src="' + filer.fs.root.toURL() + IMAGESFOLDER + item.uri + ".png" + '" class="logo" title="Open file" alt="Open model" ' + 
	   ' onError="this.onerror=null;this.src=\'images/icons/file.png\';"' +      
	   ' onClick="$(\'#viewmodel\').attr(\'src\', \'index.html?uri=' + mFolder[index].uri + '\');">');
      html.push('</div>');
  }
  
  html.push('<div>');

	  // imágenes de los autores
	  html.push('<div data-authorsimg class="data-authorsimg">');
	  if(item.authorimg) {
	  	var imgnum = item.authorimg.split(';').length;
	  	if(imgnum > 0) {
	  		var src = filer.fs.root.toURL() + PHOTOSFOLDER + item.uri + 0 + ".png";
	  		html.push('<img style="display: inline;" src="' + src + '"/>');  		
	  	}
	  	for(var i=1; i<imgnum; i++) {  		
	  		var src = filer.fs.root.toURL() + PHOTOSFOLDER + item.uri + i + ".png";
	  		html.push('<img style="display: none;" src="' + src + '"/>');
	  	}
	  }
	  html.push('</div>');

	  // title
	  (!roentry && (item.type == 1))?
	  		html.push('<div data-filename ondblclick="toggleContentEditable(this)" onblur="renameFolder(this, ', index, ')" title="Double-click to rename this entry">' + item.name + '</div>') :
	  		html.push('<div data-filename>' + item.name + '</div>');
	  
	  // nombres de los autores
	  html.push('<div data-authors class="data-authors">');
	  if(item.author) {
	  	var authors = item.author.split(';');
	  	
	  	if(authors.length > 0) {
	  		html.push('<span style="display: inline;">' + authors[0] + '</span>');  		
	  	}
	  	for(var i=1; i<authors.length; i++) {  		
	  		html.push('<span style="display: none;">' + authors[i] + '</span>');  		
	  	}
	  }
	  html.push('</div>');
	  
	  // buttons
	  if(item.type == 0) {
	    html.push('<a href="javascript:" data-preview-link onclick="readFile(' + index + ')"><img src="images/icons/library.png" class="icon" title="Information" alt="Information"></a>');
	    html.push('<a download="' + item.name + '.zip" href="' + filer.fs.root.toURL() + BASEFOLDER + item.uri + ".zip" + '" download><img src="images/icons/download.png" class="icon" title="Download" alt="Download"></a>');
	        	    
	    html.push('<a href="index.html?uri=' + mFolder[index].uri + '" target="_blank" newtab>');
	    html.push('<img ix="' + index + '" src="images/icons/newtab.png" class="icon" title="Open model in new tab" alt="Open model in new tab"></a>');
	  }	  
	  if(!readonly)
	  	html.push('<a href="javascript:removeEntry(\'' + addslashes(item.name) + '\',' + index + ')" data-remove-link><img src="images/icons/trash_empty.png" class="icon" title="Remove" alt="Remove"></a>');
	  	  	  
  html.push('</div>');
  

  return html.join('');
}

function loadFolderFromPath(uri,pathIx,rofolder) {		
	if(uri == "") { // root
		readonly = false;
		path = [];
   		initModelManager(function() { refreshFolder(); });
   } else {
		// folder de solo lectura
		readonly = rofolder;	
		
		// muestra folder actual
	    path.splice(pathIx+1, path.length-pathIx);
	   	
	   	changeFolder(uri, function() { refreshFolder(); });		
   }
   
   viewmodel.src = "about:blank";
   viewmodel.style.display = "none";
}

function loadFolder(index) {
	var item = mFolder[index];

	// folder de solo lectura
	readonly = item.readonly && (item.readonly == "true");	
	
	// añade folder actual
    path.push(item);
   	
   	changeFolder(item.uri, function() { refreshFolder(); });
}

function openFile(uri) {
  readAllMetadata(uri, function(map) {
  	var pages = [];
  	var count = 0;
  	
   	var modelkey = MODEL_KEY;
   	var model = map[MODEL_KEY];    	  
   	if(model != null) {
	    var w = window.open('', '_self');
	    var simhtml = SIMHTML2.join('').replace("%title%", map[NAME_KEY]);	    
   		
   		// internal models
       	var index = 1;
        while(model != null) {
        	// add webview for model
        	var internalmap = map[INTERNAL_KEY][model];	        	

		    if(internalmap[PAGE_KEY]) {
		    	count++;
		    	var tag = filer.fs.root.toURL() + BASEFOLDER + uri + "/" + model + "/" + internalmap[PAGE_KEY];
		    	var title = internalmap[TITLE_KEY]?internalmap[TITLE_KEY]:"Page " + count;
		    	if(index == 1) {
		    		pages.push('<li><a href="#" pos="first" tag="' + tag + '">' + title + '</a></li>');
		    		simhtml = simhtml.replace("%sim%", tag);
		    	} else {
		    		pages.push('<li><a href="#" pos="' + count + '" tag="' + tag + '">' + title + '</a></li>');
		    	}		    	
		    	
			    for(var i=0;internalmap[PAGE_KEY + i];i++) {
			    	count++;
			    	var tag = filer.fs.root.toURL() + BASEFOLDER + uri + "/" + model + "/" + internalmap[PAGE_KEY + i];
			    	var title = internalmap[TITLE_KEY + i]?internalmap[TITLE_KEY + i]:"Page " + count;
			    	if(internalmap[PAGE_KEY + (i+1)] || map[MODEL_KEY + index])
			    		pages.push('<li><a href="#" pos="' + count + '" tag="' + tag + '">' + title + '</a></li>');
			    	else
			    		pages.push('<li><a href="#" pos="last" tag="' + tag + '">' + title + '</a></li>');
			    }    	 			    
		    }
        	
           	modelkey = MODEL_KEY + index;
           	model = map[modelkey];
           	index++;
        }	    	        
	    simhtml = simhtml.replace("%pages%",pages.join(''));
	    w.document.write(simhtml);
	    w.document.close();        
   	} else {
   		// unique model
	  	if(!map[PAGE_KEY + '0']) { // si sólo hay una paǵina, la abro directamente
	  		self.open(filer.fs.root.toURL() + BASEFOLDER + uri + "/" + map[MAINSIM_KEY], "_self");
	
	   	} else { // sino creo un menú para poder cambiar de una a otra
		    var w = window.open('', '_self');
		    var simhtml = SIMHTML2.join('').replace("%title%", map[NAME_KEY]);	    
		    
		    var pages=[];
		    if(map[PAGE_KEY]) {
		    	var tag = filer.fs.root.toURL() + BASEFOLDER + uri + "/" + map[PAGE_KEY];
		    	pages.push('<li><a href="#" pos="first" tag="' + tag + '">' + map[TITLE_KEY] + '</a></li>');
		    	
		    	simhtml = simhtml.replace("%sim%", tag);
		    	
			    for(var i=0;map[PAGE_KEY + i];i++) {
			    	var tag = filer.fs.root.toURL() + BASEFOLDER + uri + "/" + map[PAGE_KEY + i];
			    	if(map[PAGE_KEY + (i+1)])
			    		pages.push('<li><a href="#" pos="' + (i+1) + '" tag="' + tag + '">' + map[TITLE_KEY + i] + '</a></li>');
			    	else
			    		pages.push('<li><a href="#" pos="last" tag="' + tag + '">' + map[TITLE_KEY + i] + '</a></li>');
			    }    	 
			    
		    }

		    simhtml = simhtml.replace("%pages%",pages.join(''));
		    w.document.write(simhtml);
		    w.document.close();		    
		}
   	}
   	
  });  	
}
	    
/*
   	} else { // sino creo un menú para poder cambiar de una a otra
	    var w = window.open('', '_self');
	    var simhtml = SIMHTML.join('').replace("%title%", map[NAME_KEY]);
	    simhtml = simhtml.replace("%sim%", filer.fs.root.toURL() + BASEFOLDER + uri + "/" + map[MAINSIM_KEY]);
	    
	    var pages=[];
	    if(map[PAGE_KEY]) {
	    	var tag = filer.fs.root.toURL() + BASEFOLDER + uri + "/" + map[PAGE_KEY];
	    	pages.push('<li style="border: solid 1px gray; border-bottom: 0;"><a href="#" tag="' + tag + '">' + map[TITLE_KEY] + '</a></li>');
	    	
		    for(var i=0;map[PAGE_KEY + i];i++) {
		    	var tag = filer.fs.root.toURL() + BASEFOLDER + uri + "/" + map[PAGE_KEY + i];
		    	pages.push('<li style="border: solid 1px gray; border-bottom: 0;"><a href="#" tag="' + tag + '">' + map[TITLE_KEY + i] + '</a></li>');
		    }    	 
	    }
	    simhtml = simhtml.replace("%pages%",pages.join(''));
	    w.document.write(simhtml);
	    w.document.close();
	}  		
  });             	  	 
*/

function renameFolder(el, index) {
  	errors.textContent = ''; // Reset errors.
	
	var item = mFolder[index];
	changeNameFolder(item, el.innerText, function() { refreshFolder(); });
}

function removeEntry(name, index) {
  errors.textContent = ''; 

  if (!confirm('Delete ' + delslashes(name) + '?')) {
    return;
  }
  
  delItem(index, function() { refreshFolder(); });
}

function onImportFile(e) {
  var files = e.target.files;
  if (files.length) {
    var count = 0;
    Util.toArray(files).forEach(function(file, i) {
    	if (VIEWABLE_FILES.indexOf(Util.getFileExtension(file.name)) != -1) {
    		errors.textContent = "Loading - Please wait.";
   			addItemFromBlobFile(file, function() { refreshFolder(); } );
   		} else {
			onError("File not supported!");
   		}
    });
  }
}

function downloadComPADREFile(uri, callback, onerror) {
    if (uri.toUpperCase().indexOf("COMPADRE") != -1) {
    	var url = 'osp.php?url=' + encodeURIComponent(uri); 
    	addItemFromHttpFile(url, function() { refreshFolder(); callback(); } );
	} else {
		onerror("File not supported!");
	}	
}

function downloadHttpFile(uri, callback, onerror) {
    if (VIEWABLE_FILES.indexOf(Util.getFileExtension(uri)) != -1) {
    	var url = 'cloud.php?url=' + encodeURIComponent(uri); 
    	addItemFromHttpFile(url, function() { refreshFolder(); callback(); } );
	} else {
		onerror("File not supported!");
	}	
}

function addNewFolder() {
  var name = document.querySelector('#entry-name').value;
  if (name) {
  	addFolder(name, function() { refreshFolder(); });
  }
}

function toggleContentEditable(el) {
  if (el.isContentEditable) {
    el.removeAttribute('contenteditable');
  } else {
    el.setAttribute('contenteditable', '');
    el.focus();
  }
}

function readFile(index) {
  var item = mFolder[index];
  errors.textContent = ''; // Reset errors.

  filePreview.classList.toggle('show');
  
  // elemento cabecera
  var header = document.createElement('div')
  
  // nombre modelo
  var innerHTML = [
    '<b>', item.name, '</b>'  
  ];

  // nombre de autores
  innerHTML.push('<br>', item.author);
  
  // copyright si existe
  if(item.copyright) innerHTML.push('<br>', 'Copyright: ', item.copyright);

  // inserto html en cabecera
  innerHTML.push('<br><br>');
  header.innerHTML = innerHTML.join('');

  // imágenes de los autores
  if(item.authorimg) {
  	var imgnum = item.authorimg.split(';').length;
  	for(var i=0; i<imgnum; i++) {
  		var authors = document.createElement('img');
  		authors.src = filer.fs.root.toURL() + PHOTOSFOLDER + item.uri + i + ".png";
  		authors.style.margin = "0px 10px";
  		header.appendChild(authors);
  	}
  }

  // inserto cabecera
  filePreview.appendChild(header); 

  // imagen del modelo
  var img = document.createElement('img');
  img.src = filer.fs.root.toURL() + IMAGESFOLDER + item.uri + ".png";
  filePreview.appendChild(img);
  
}

function onKeydown(e) {
  var target = e.target;

  // Prevent enter key from inserting carriage return in the contenteditable
  // file/folder renaming.
  if (target.isContentEditable && 'filename' in target.dataset) {
    if (e.keyCode == 13) { // Enter
      e.preventDefault();
      e.stopPropagation();
      target.blur();
    }
    return;
  }

  if (e.keyCode == 27) { // ESC  	
    filePreview.classList.remove('show');
    filePreview.innerHTML = '';

    librariesNav.classList.remove('show');
    librariesNav.innerHTML = '';

    e.preventDefault();
    e.stopPropagation();
    return;
  }
}

function onRestartApp() {
  if (!confirm('Delete all current models and restart the web application?')) {
    return;
  }
  restartModelManager(function() { refreshFolder(); });
}   

function onVisitDLibraries() {
  librariesNav.classList.toggle('show');
  
  var html = [];
  // select for DLs
  html.push('<div title>');
  html.push('<span>Select Digital Library: </span>');
  html.push('<select id="selectDL" onchange="changeDigitalLibrary()" name="selectDL">');
  for(var i=0; i<DIGITAL_LIBRARY.length; i++)
  	html.push('<option value="' + DIGITAL_LIBRARY[i][1] + '">' + DIGITAL_LIBRARY[i][0] + '</option>');
  html.push('</select>');
  html.push('</div>');
  
  // iframe for DL
  html.push('<iframe id="digitalLibrary" src="' + "osp.html" + '"/>');
  librariesNav.innerHTML = html.join('');  
}   

function changeDigitalLibrary () { 
	var select = document.getElementById("selectDL");
	
	var iframe = document.getElementById("digitalLibrary");
	var url = select.options[select.selectedIndex].value;
	if(select.selectedIndex == 0) { // OSP
		iframe.src = url;
	} else { // PHP
		iframe.src = "cloudphp.html?digliburl="+encodeURIComponent(url);
	}
}

function onChangeGrid() {
  if(filesContainer.className == "large") 
  	filesContainer.className = "small";
  else
  	filesContainer.className = "large";
}

function onBackup() {
  backupMemory(function(file) {
  	console.log("ok " + file);
  });
}

function onLoadViewModel() {
	if(viewmodel.src == "about:blank") {
		viewmodel.style.display = "none";
		filesContainer.style.display = "inline-block";
	} else {
		viewmodel.style.display = "inline-block";
		filesContainer.style.display = "none";
	}	
}

function addListeners() {
  // eventos asociados a botones
  backupButton.addEventListener('click', onBackup, false);
  gridButton.addEventListener('click', onChangeGrid, false);
  restartButton.addEventListener('click', onRestartApp, false);
  libraryButton.addEventListener('click', onVisitDLibraries, false);
  importButton.addEventListener('click', function() { importFileInput.click(); }, false);
  importFileInput.addEventListener('change', onImportFile, false);
  if(viewmodel) viewmodel.addEventListener('load', onLoadViewModel, false);
  document.addEventListener('keydown', onKeydown, false);

  var dnd = new DnDFileController('body', function(files, e) {
  	if(readonly) {
  		onError("Folder is readonly!");
  	} else {
  		if(files.length > 0) {
  			// drag&drop with files
		    var items = e.dataTransfer.items;
		    for (var i = 0, item; item = items[i]; ++i) {
		
			  filer.open(item.webkitGetAsEntry(), function(file) {      	      	
		    	if (VIEWABLE_FILES.indexOf(Util.getFileExtension(file.name)) != -1) {
		    		errors.textContent = "Loading - Please wait.";
		   			addItemFromBlobFile(file, function() { refreshFolder(); } );
		   		} else {
					onError("File not supported!");
		   		}        
		      });		
		    }
		} else {
			// consigo la primera imagen
			var div = document.createElement('div');
			div.innerHTML = e.dataTransfer.getData("text/html");
			var srcEle = div.getElementsByClassName('logo')[0];
			// consigo la segunda imagen
			var toEle = e.toElement;			
			if(srcEle && toEle) {
			 if (srcEle.className == 'logo' && toEle.className == 'logo') {
	  			// internal drag&drop
				var srcIx = srcEle.getAttribute("ix");
				var toIx = toEle.getAttribute("ix");
				
				if(mFolder[toIx].readonly && mFolder[toIx].readonly == "true") {
  					onError("Folder is readonly!");
  				} else {				
					moveItem(srcIx,toIx,function() { refreshFolder(); });
				}
			 } else if (srcEle.className == 'logo' && toEle.className == 'pathFolder') {
	  			// internal drag&drop to path
				var srcIx = srcEle.getAttribute("ix");
				var toUri = toEle.getAttribute("uri");
			 	
			 	moveItemUsingUri(srcIx,toUri,function() { refreshFolder(); });
			 }	
			}
		}  		
  	}
  });
}

function addslashes(str) {
  return (str + '')
    .replace(/[\\"']/g, '\\$&');
}

function delslashes(str) {
  return (str + '')
  	.replace(/\\/g, '');
}

// manage changing author's images
function changeAuthors(){
	// change authors
	var eles = document.getElementsByClassName('data-authors');
	for(var i=0; i<eles.length; i++) {
		var c = eles[i].childNodes;
		for(var j=0; j<c.length; j++) {
			if(c[j].style.display == "inline") {
				c[j].style.display = "none";
				if(c[j].nextSibling) 
					c[j].nextSibling.style.display = "inline";
				else
				  	c[0].style.display = "inline";
				break;
			}
		}
	}

	// change authors
	var eles = document.getElementsByClassName('data-authorsimg');
	for(var i=0; i<eles.length; i++) {
		var c = eles[i].childNodes;
		for(var j=0; j<c.length; j++) {
			if(c[j].style.display == "inline") {
				c[j].style.display = "none";
				if(c[j].nextSibling) 
					c[j].nextSibling.style.display = "inline";
				else
				  	c[0].style.display = "inline";
				break;
			}
		}
	}
}

setInterval(changeAuthors, 2000);

window.addEventListener('DOMContentLoaded', function(e) {
  addListeners();

  var count = 0;
  setInterval(function() {
    ticker.innerHTML =
        'Tip: ' + TICKER_LIST[count++ % TICKER_LIST.length];
    ticker.classList.add('fadedIn');
  }, 3000);
}, false);
	
