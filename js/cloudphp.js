/** 
 * Copyright 2015 - Félix J. García
 * 
 * @author Félix J. García (fgarcia@um.es)
 */

/* 
 * library
 */
cloudPHPLibrary = function() {
	self = {};
	var mUrl;

	self.getNameFromId = function(id) {
		return id.substr(id.lastIndexOf("/")+1);
	}
	
	/**
	 * Returns the LibraryCollection for a given EjsS path (query).
	 * 
	 * @return the collection
	 */
	self.getCollection = function(doc,url) {
		mUrl = url;
		return xmlToJson(doc);
	}

	// Changes XML to JSON
	function xmlToJson(xml, parentName, parentUrl) {
		// Create the return object
		var coljson = [];
				
		if (xml.nodeType == 1) { // element
			var json = {};
			
			var nodeName = xml.nodeName;
			if(nodeName.toUpperCase() == "DIR") {
				json = toJSONCollection(xml, parentName);
			} else if(nodeName.toUpperCase() == "MODEL") {
				json = toJSONModel(xml, parentName, parentUrl);
			}				
			
			// include element
			if(json.id) coljson.push(json);			
		
			// do children
			if (xml.hasChildNodes()) {			
				for(var i = 0; i < xml.childNodes.length; i++) {
					var item = xml.childNodes.item(i);
	
					// it is model or has any model				
					var models = item.getElementsByTagName?item.getElementsByTagName("model"):0;
					if((models && models.length > 0) || item.nodeName.toUpperCase() == "MODEL") {						
						var name = json.id?json.id:"#"; // first element is BODY
						var url = json.data && json.data.url?json.data.url:"";
						var itemjson = xmlToJson(item,name,url);
						if(itemjson.length > 0) coljson = coljson.concat(itemjson);
					}
				}
			}			
		}	
		
		return coljson;
	}

	/***
	 * To JSON
	 */	
	function toJSONModel(xml, parentName, url) {
		var json = {};

		// do attributes
		if (xml.attributes.length > 0) {					
			json["parent"] = parentName;
			json["type"] = "file";
			json["text"] = xml.getAttribute("name")?xml.getAttribute("name"):"EjsS Model";
			json["id"] = parentName + "/" + json["text"];
			json["data"] = {};
			json["data"]["target"] = xml.getAttribute("zip");
			json["data"]["html"] = xml.getAttribute("html");
			json["data"]["url"] = url;		
		}				

		return json;
	}

	/***
	 * To JSON
	 */	
	function toJSONCollection(xml, parentName) {
		var json = {};
		
		// do attributes
		if (xml.attributes.length > 0) {					
			json["parent"] = parentName;
			json["type"] = "folder";
			json["text"] = xml.getAttribute("name")?xml.getAttribute("name"):"EjsS Digital Collection";
			json["id"] = parentName + "/" + json["text"];
			json["data"] = {};
			json["data"]["html"] = xml.getAttribute("html");
			json["data"]["url"] = xml.getAttribute("url");		
		}
		
		return json;
	}

	return self;
}();

/*
 * Manage the download of the library from url
 */
cloudPHPConnection = function(url, callback_ok, callback_progess, callback_error) {
	var lUrl = "cloud.php?url=" + encodeURIComponent(url + "/indexEJSSdl.php");
    // console.log(url);
    
    // connection
    var query = lUrl.split("?");
	var http = new XMLHttpRequest();
	http.onprogress = callback_progess;
	http.responseType = "document";
	http.open("POST", query[0], true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	http.onreadystatechange = function() { // call a function when the state changes.
	    if(http.readyState == 4) {
	    	if (http.status == 200 && http.responseXML && http.responseXML.documentElement) {
	    		// add elements into treeBuilder
	    		var doc = http.responseXML.body;
		    	var library = cloudPHPLibrary.getCollection(doc,url);	
				callback_ok(library);		    			
	    	} else {
	    		console.log("No XML content in EjsS library connection!");
	    		callback_error();
	    	}	        
	    }
	}
	http.send(query[1]);
}




