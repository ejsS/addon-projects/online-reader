<?php
    
    /** ComPADRE Wrapper **/
    if(isset($_GET["url"])) {
    	/** downloading files **/
		$url = $_GET["url"];
		
		$result = file_get_contents($url);
		
    	// header('Content-Description: File Transfer');
    	// header('Content-Type: application/zip');
    	header('Content-Disposition: attachment; filename=ospModel.zip');
    	header('Expires: 0');
    	header('Cache-Control: must-revalidate');
    	header('Pragma: public');
		header("Content-Encoding: none");
		header("Content-length: " . strlen($result));
		
		echo($result);
		
	} else {
		/** rest of commands **/
		if(!isset($_POST["basename"])) return;
		if($_POST["basename"] != "tree" && $_POST["basename"] != "records") return;
		
		if($_POST["basename"] == "tree") {
			$data = array('verb' => 'Identify', 'OSPType' => 'EJSS Model');
		} else if($_POST["basename"] == "tree") {
			$data = array('OSPType' => 'EJSS Model');
		}
		
		$url = 'http://www.compadre.org/services/rest/osp_ejss.cfm';
		
		foreach( $_POST as $key => $val ) {
			if($key != "basename") {
				$data[$key] = $val;
			}
		}	
		$url = $url . "?" . http_build_query($data);
		
		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    ),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		
		echo($result);			
	}

?>
