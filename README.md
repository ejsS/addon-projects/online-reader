# Online viewer for EjsS Simulations

## Getting Started

Download and move the online viewer into the web server's document root or your public HTML directory.

Open it with your web browser (Chrome is strongly recommended) and enjoy it!

## Author

* **Félix J. García-Clemente** - [CV] (https://webs.um.es/fgarcia/)


