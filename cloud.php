<?php

	if(isset($_POST["url"])) {
		$url = $_POST["url"];
		
		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'GET'
		    ),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		
		echo($result);
	
	} else if(isset($_GET["url"])) {
		$url = $_GET["url"];
		
		$result = file_get_contents($url);
		
    	// header('Content-Description: File Transfer');
    	// header('Content-Type: application/zip');
    	header('Content-Disposition: attachment; filename=ospModel.zip');
    	header('Expires: 0');
    	header('Cache-Control: must-revalidate');
    	header('Pragma: public');
		header("Content-Encoding: none");
		header("Content-length: " . strlen($result));
		
		echo($result);	
		
	} else return;	
	
?>